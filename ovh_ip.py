#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ovh_ip, an Ansible module for managing OVH IPs
# Copyright (C) 2019, Marc Simonetti <marc.simonetti@ipreso.com>
#
# Inspired from the work of gheesh and its ansible-ovh-dns module:
# https://github.com/gheesh/ansible-ovh-dns
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

from __future__ import print_function

DOCUMENTATION = '''
---
module: ovh_ip
author: Marc Simonetti
short_description: Manage OVH IPs
description:
    - Manage OVH (French European hosting provider) IPs
requirements: [ "ovh" ]
options:
    target_ip:
        required: true
        description:
            - IP to manage
    value:
        required: true
        description:
            - Name of the DNS record
    state:
        required: false
        default: present
        choices: ['present', 'absent']
        description:
            - Determines wether the record is to be created/modified or deleted
'''

EXAMPLES = '''
# Assign a reverse name to an IP
- ovh_ip: state=present value=test.example.org target_ip=10.10.10.10

# Delete an existing association
- ovh_ip: state=absent target_ip=10.10.10.10
'''

import os
import sys

try:
    import ovh
except ImportError:
    print("failed=True msg='ovh required for this module'")
    sys.exit(1)


# TODO: Try to automate this in case the supplied credentials are not valid
def get_credentials():
    """This function is used to obtain an authentication token.
    It should only be called once."""
    client = ovh.Client()
    access_rules = [
        {'method': 'GET', 'path': '/ip/*'},
        {'method': 'GET', 'path': '/ip'},
        {'method': 'PUT', 'path': '/ip/*'},
        {'method': 'POST', 'path': '/ip/*'},
        {'method': 'DELETE', 'path': '/ip/*'},
    ]
    validation = client.request_consumerkey(access_rules)
    print("Your consumer key is {}".format(validation['consumerKey']))
    print("Please visit {} to validate".format(validation['validationUrl']))


def get_target_block(client, target_ip):
    """Obtain the IP block of an IP"""
    info = client.get('/ip', ip=target_ip)
    return info

def get_target_record(client, target_block, target_ip):
    """Obtain the current reverse for a specific IP"""
    try:
        info = client.get('/ip/{}/reverse/{}'.format(target_block.replace("/","%2F"), target_ip))
    except ovh.exceptions.ResourceNotFoundError as e:
        # IP does not have a reverse
        return ''
    return info['reverse']


def main():
    module = AnsibleModule(
        argument_spec = dict(
            target_ip = dict(required=True),
            value = dict(default=''),
            state = dict(default='present', choices=['present', 'absent']),
        ),
        supports_check_mode = True
    )

    # Get parameters
    target_ip       = module.params.get('target_ip')
    value           = module.params.get('value')
    state           = module.params.get('state')

    # Connect to OVH API
    client = ovh.Client()

    # Check that the target_block exists
    blocks = get_target_block (client, target_ip)
    if len(blocks) < 1:
        module.fail_json(msg='IP {} does not exist on our OVH account'.format(target_ip))
    target_block = blocks[0]

    # Obtain the current reverse DNS of this IP
    current_reverse = get_target_record(client, target_block, target_ip)

    # Remove an association
    if state == 'absent':
        # Are we done yet?
        if not current_reverse:
            module.exit_json(changed=False)

        if not module.check_mode:
            # Remove the record
            client.delete('/ip/{}/reverse/{}'.format(target_block.replace("/","%2F"),
                                                     target_ip))
        module.exit_json(changed=True)

    # Add / modify a record
    if state == 'present':

        # Since we are inserting a record, we need a target
        if value == '':
            module.fail_json(msg='Did not specify a reverse name')

        # Does the record exist already?
        if current_reverse == '{}.'.format(value):
            # The record is already as requested, no need to change anything
            module.exit_json(changed=False)

        # Update the record
        if not module.check_mode:
            #client.delete('/ip/{}/reverse/{}'.format(target_block.replace("/","%2F"),
            #                                         target_ip))
            client.post('/ip/{}/reverse'.format(target_block.replace("/","%2F")),
                        ipReverse=target_ip, reverse=value)

            module.exit_json(changed=True)

    # We should never reach here
    module.fail_json(msg='Internal ovh_dns module error')


# import module snippets
from ansible.module_utils.basic import *

main()
