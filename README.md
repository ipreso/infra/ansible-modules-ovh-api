Installation
=============

In order to use some OVH API functions, you have to create an authentication token:
https://docs.ovh.com/gb/en/customer/first-steps-with-ovh-api/

Then install the required software:
```
$ sudo apt-get install python-pip
$ sudo pip install ovh
```

Request the consumer key:
```
cat <<EOF | python
import ovh
client = ovh.Client()
access_rules = [
  {'method': 'GET', 'path': '/domain/*'},
  {'method': 'PUT', 'path': '/domain/*'},
  {'method': 'POST', 'path': '/domain/*'},
  {'method': 'DELETE', 'path': '/domain/*'},
  {'method': 'GET', 'path': '/ip/*'},
  {'method': 'GET', 'path': '/ip'},
  {'method': 'PUT', 'path': '/ip/*'},
  {'method': 'POST', 'path': '/ip/*'},
  {'method': 'DELETE', 'path': '/ip/*'},
]
validation = client.request_consumerkey(access_rules)
print("Your consumer key is {}".format(validation['consumerKey']))
print("Please visit {} to validate".format(validation['validationUrl']))
EOF
```

Then, please create `/etc/ovh.conf` with the newly created credentials:

```
[default]
endpoint=ovh-eu

[ovh-eu]
application_key=XXX
application_secret=YYY
consumer_key=ZZZ
```

Installation
============

Copy the module files into the Ansible module directory, for example:
```
cp ovh*py /usr/lib/python3/dist-packages/ansible/modules/
```
